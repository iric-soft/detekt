/**
 * Created by jpl on 25/01/16.
 */
'use strict';

var gulp = require('gulp');
var electron = require('electron-connect').server.create();
var packager = require('electron-packager');

gulp.task('serve', function () {

    // Start browser process
    electron.start();

    // Restart browser process
    gulp.watch(['index.js', 'index.html'], electron.restart);

    // Reload renderer process
    //gulp.watch('index.html', electron.reload);
});

function build_done(err, appPath) {
    if (err !== null) {
        console.log(err);
    } else {
        console.log('Build successful !\n Apps are here: ' + appPath);
    }
}

gulp.task('build-linux', function() {
    var opts = {
        platform: 'linux',
        arch: 'x64',
        version: '0.36.5',
        dir: '.',
        out: 'dist',
        ignore: 'dist',
        overwrite: true
    };
    packager(opts, build_done);
});

gulp.task('build-darwin', function() {
    var opts = {
        platform: 'darwin',
        arch: 'x64',
        version: '0.36.5',
        dir: '.',
        out: 'dist',
        ignore: 'dist',
        overwrite: true
    };
    packager(opts, build_done);
});

gulp.task('build-win32', function() {
    var opts = {
        platform: 'win32',
        arch: 'x64',
        version: '0.36.5',
        dir: '.',
        out: 'dist',
        ignore: 'dist',
        overwrite: true
    };
    packager(opts, build_done);
});

gulp.task('build-all', function() {
    var opts = {
        platform: 'all',
        arch: 'x64',
        version: '0.36.5',
        dir: '.',
        out: 'dist',
        ignore: 'dist',
        overwrite: true
    };
    packager(opts, build_done);
});

gulp.task('default', ['serve']);