/**
 * Created by jpl on 21/01/16.
 */

'use strict';

const electron = require('electron');
const app = electron.app;  // Module to control application life.
const BrowserWindow = electron.BrowserWindow;  // Module to create native browser window.
var Menu = require("menu");

// Jellyfish binary path
var binary = __dirname + '/resources/jellyfish/bin/jellyfish';
if (process.platform == 'darwin') {
    binary = __dirname + '/resources/jellyfish/bin/jellyfish_mac';
} else if (process.platform =='win32') {
    binary = __dirname + '/resources/jellyfish/bin/jellyfish.exe';
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
var mainWindow = null;

// Quit when all windows are closed.
app.on('window-all-closed', function() {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform != 'darwin') {
        app.quit();
    }
});

function newBrowserWindow() {
    mainWindow = new BrowserWindow({
        width: 650,
        height: 600
    });

    // and load the index.html of the app.
    mainWindow.loadURL('file://' + __dirname + '/index.html');

    // Open the DevTools.
    //mainWindow.webContents.openDevTools();

    // Emitted when the window is closed.
    mainWindow.on('closed', function() {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });

    // Create the Application's main menu
    var template = [{
        label: "Application",
        submenu: [
            { label: "About Application", role: "about" },
            { type: "separator" },
            { label: "Close Window", accelerator: "Command+W", click: function() { mainWindow.close(); }},
            { label: "Quit", accelerator: "Command+Q", click: function() { app.quit(); }}
        ]}, {
        label: "Edit",
        submenu: [
            { label: "Undo", accelerator: "CmdOrCtrl+Z", role: "undo:" },
            { label: "Redo", accelerator: "Shift+CmdOrCtrl+Z", role: "redo:" },
            { type: "separator" },
            { label: "Cut", accelerator: "CmdOrCtrl+X", role: "cut:" },
            { label: "Copy", accelerator: "CmdOrCtrl+C", role: "copy:" },
            { label: "Paste", accelerator: "CmdOrCtrl+V", role: "paste:" },
            { label: "Select All", accelerator: "CmdOrCtrl+A", role: "selectAll:" }
        ]}
    ];

    Menu.setApplicationMenu(Menu.buildFromTemplate(template));
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', newBrowserWindow);

// This method will be called on Macs when the dock icon is clicked
// open a new browser window if none exist
app.on('activate', function() {
    if (mainWindow == null) {
        newBrowserWindow();
    }
});

const ipcMain = require('electron').ipcMain;
var execSync = require('child_process').execSync;

ipcMain.on('asynchronous-message', function(event, arg) {
    console.log('Looking for kmer: ' + arg);  // prints "ping"
    var response = execSync(binary + ' query ' + __dirname + '/resources/jellyfish/db/test.jf ' + arg).toString();

    if ('' != response) {
        response = '<em>' + response.substr(response.lastIndexOf(" ")) + '</em>';
    } else {

        response += 'K-mer <strong><em>' + arg + '</em></strong> cannot be found.';
        response += '<br /><br />Try one of these fine alternatives:<br />';
        response += '<ul id="altkmers">';
        response += '<li class="altkmer">AGGAGTTCCTTCCTAAAACCT</li>';
        response += '<li class="altkmer">CTGCTCTCCTCTTCAACGCTC</li>';
        response += '<li class="altkmer">AGACAGTTCTTTATGTTCTGG</li>';
        response += '</ul>';
    }
    event.sender.send('asynchronous-reply', response);
});
