# deteKt demo application

## Usage instructions for precompiled binaries
Binaries for all 3 major operating systems are available in the [Downloads section](https://bitbucket.org/iric-soft/detekt/downloads)  

Please ensure your operating system is supported.  
See OS support here: [Electron supported platforms](http://electron.atom.io/docs/v0.36.5/tutorial/supported-platforms/)
#### Windows:
* Download deteKt_x64.zip
* Unzip the folder 
* Start the application by double-clicking deteKt.exe
#### Linux
* Download deteKt-linux-x64.tar.gz
* Then, using your shell:
```bash
> tar -zxvf deteKt-linux-x64.tar.gz
> cd deteKt-linux-x64
> ./deteKt

# If you cannot launch the application, make sure the deteKt file is executable
> chmod u+x deteKt
```
#### Mac OS (Darwin)
* Download deteKt_x64.dmg
* Double-click deteKt_x64.dmg
* Start the application by double-clicking the deteKt application  

If you get a security warning stating that the application is from an unidentified developer, do the following:  

* CTRL-click the application and choose "Open" from the dialog, you will now be able to launch deteKt

# Downloading and working with the source code
## Pre-requisites
- node.js / npm
- Global installations of both [*electron-prebuilt*](https://www.npmjs.com/package/electron-prebuilt) and [*gulp*](https://www.npmjs.com/package/gulp): 
```bash
> npm install -g electron-prebuilt
> npm install -g gulp
```


## Installation
Will install all necessary dependencies.
```bash
# clone this repo
# then, from within the app folder:
> npm install
```

## Run application
```bash
# from within the app folder:
> npm start
```

## Run application in Development mode
Running in development mode will reload the app when changes are detected on index.js or index.html  
Makes use of [*gulp*](https://www.npmjs.com/package/gulp) 
```bash
# from within the app folder:
> gulp
```